Categories:Office
License:GPLv3+
Web Site:https://github.com/zikalify/BMI_Calculator
Source Code:https://github.com/zikalify/BMI_Calculator
Issue Tracker:https://github.com/zikalify/BMI_Calculator/issues

Auto Name:BMI Calculator
Summary:Body Mass Index calculator
Description:
Calculate your Body Mass Index based on the weight and height you enter.
Results are the same as the calculator provided by the UK's NHS.
.

Repo Type:git
Repo:https://github.com/zikalify/BMI_Calculator.git

Build:1.0,1
    commit=1.0
    subdir=BMI
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1

