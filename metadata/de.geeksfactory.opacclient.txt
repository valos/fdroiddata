Categories:Reading
License:MIT
Web Site:http://opacapp.de
Source Code:https://github.com/raphaelm/opacclient
Issue Tracker:https://github.com/raphaelm/opacclient/issues

Auto Name:Web Opac
Summary:German public libraries
Description:
Client for some German, Austrian and Swiss public libraries that offer online catalogues.
.

Repo Type:git
Repo:https://github.com/raphaelm/opacclient.git

Build:2.0.18,54
    commit=2.0.18
    submodules=yes
    init=rm -rf 3dparty/SlidingMenu/example && \
        (cd 3dparty/HoloEverywhere && \
        git submodule update --init && \
        $$SDK$$/tools/android update project -p contrib/ActionBarSherlock/actionbarsherlock)
    update=.,3dparty/HoloEverywhere/library,3dparty/SlidingMenu/library

Build:2.0.30,66
    commit=2.0.30
    submodules=yes
    init=rm -rf 3dparty/SlidingMenu/example && \
        (cd 3dparty/HoloEverywhere && \
        git submodule update --init && \
        $$SDK$$/tools/android update project -p contrib/ActionBarSherlock/actionbarsherlock)

Build:2.0.31,68
    commit=2.0.31
    submodules=yes
    prebuild=rm -rf 3dparty/SlidingMenu/example

Build:2.0.32,69
    commit=2.0.32
    submodules=yes
    prebuild=rm -rf 3dparty/SlidingMenu/example

Build:2.1.0,70
    commit=2.1.0
    submodules=yes
    prebuild=rm -rf 3dparty/SlidingMenu/example

Build:3.0.1,74
    commit=3.0.1
    submodules=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:3.0.3
Current Version Code:76

