Categories:Navigation
License:GPLv3
Web Site:http://osmand.net
Source Code:https://github.com/osmandapp/Osmand
Issue Tracker:https://github.com/osmandapp/Osmand/issues
Donate:https://code.google.com/p/osmand/#Please_support_the_project

Auto Name:OsmAnd~
Summary:Offline/online maps and navigation
Description:
Osmand~'s features can be extended by enabling the plugins via the settings,
which include online maps from many sources, tracking, OSM editing and
accessibility enhancements.

Map data of both vector and raster types can be stored on the phone memory
card for offline usage, and navigation by default uses offline methods.  Map
data packages for many territories can be downloaded from within the app and
there is a desktop program available on the website as well for creating your
own.
.

#Repo Type:git
#Repo:https://github.com/osmandapp/Osmand
Repo Type:git
Repo:https://github.com/mvdan/OsmAnd-submodules

# Old builds with the old repo
#Build:0.6.5,34
#    commit=v0.6.5
#    subdir=OsmAnd
#    encoding=utf-8
#    prebuild=mkdir assets && \
#        mkdir raw
#
#Build:0.6.6,36
#    commit=v0.6.6_2
#    subdir=OsmAnd
#    encoding=utf-8
#    prebuild=mkdir raw
#
#Build:0.6.7,37
#    commit=v0.6.7
#    subdir=OsmAnd
#    encoding=utf-8
#    patch=code37.patch
#    prebuild=mkdir raw
#
#Build:0.6.8,39
#    commit=v0.6.8
#    subdir=OsmAnd
#    encoding=utf-8
#    prebuild=mkdir raw
#
#Build:0.6.8',41
#    disable=No corresponding source for whatever this is
#    commit=unknown - see disabled
#
#Build:0.6.9,42
#    commit=v0.6.9
#    subdir=OsmAnd
#    encoding=utf-8
#    prebuild=mkdir raw
#
#Build:0.6.9',43
#    disable=No corresponding source for whatever this is
#    commit=unknown - see disabled
#
#Build:0.8.1,65
#    commit=d62472532d8
#    subdir=OsmAnd
#    target=android-8
#    init=rm -f build.xml
#    encoding=utf-8
#    forceversion=yes
#    prebuild=cd ../DataExtractionOSM && \
#        ant compile build && \
#        cd ../OsmAnd/ && \
#        sed -i 's/debuggable="true"/debuggable="false"/g' AndroidManifest.xml && \
#        cp ../DataExtractionOSM/build/OsmAndMapCreator.jar libs/ && \
#        zip -d libs/OsmAndMapCreator.jar net/osmand/LogUtil.class && \
#        cp -r ../DataExtractionOSM/build/lib/ libs/
#    buildjni=no
#
#Build:0.8.2,71
#    commit=50a4733475cd
#    subdir=OsmAnd
#    submodules=yes
#    target=android-8
#    init=rm -f build.xml
#    encoding=utf-8
#    forceversion=yes
#    forcevercode=yes
#    prebuild=sed -i 's/debuggable="true"/debuggable="false"/g' AndroidManifest.xml && \
#        cd ../DataExtractionOSM && \
#        ant compile build && \
#        cd ../OsmAnd/ && \
#        sed -i 's/app_version">[^<]*/app_version">0.8.2-fdroid/' res/values/no_translate.xml && \
#        cp ../DataExtractionOSM/build/OsmAndMapCreator.jar libs/ && \
#        zip -d libs/OsmAndMapCreator.jar net/osmand/LogUtil.class && \
#        cp -r ../DataExtractionOSM/build/lib/ libs/
#    buildjni=yes
Build:1.6.5,165
    disable=WIP
    commit=1.6.5
    subdir=android/OsmAnd
    submodules=yes
    init=rm -f ../SherlockBar/build.xml build.xml
    forcevercode=yes
    patch=analytics_165.diff
    prebuild=sed -i 's/app_version">[^<]*/app_version">1.6.5/' res/values/no_translate.xml && \
        echo -e 'NDK_TOOLCHAIN_VERSION := 4.8\nAPP_ABI := armeabi armeabi-v7a x86 mips\nOSMAND_SKIP_NEON_SUPPORT := false\nOSMAND_FORCE_NEON_SUPPORT := false\nAPP_OPTIM := release' >> jni/Application.mk
    build=sed -i 's/EXTERNAL_DEPENDENCIES=(/EXTERNAL_DEPENDENCIES=(boost-android /' full-ndk-build.sh && \
        ANDROID_SDK=$$SDK$$ ./full-ndk-build.sh && \
        ant jar native-libs -f ../../tools/OsmAndMapCreator/build.xml && \
        ant build -f ../OsmAnd-java/build.xml
    buildjni=no

Maintainer Notes:
No UCMs apply because git never contains actual releases, only pre-releses.
.

Auto Update Mode:None
Update Check Mode:None
Current Version:1.6.5
Current Version Code:165

