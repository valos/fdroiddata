Categories:Internet
License:GPLv3 or New BSD
Web Site:https://apps.wiki.tum.de/TUM+Campus+App
Source Code:https://github.com/TCA-Team/TumCampusApp
Issue Tracker:https://github.com/TCA-Team/TumCampusApp/issues

Auto Name:TUM Campus App
Summary:Information about TU München
Description:
The Technical University of Munich is one of the biggest in Germany.
This app displays:

* Events
* Messages
* Lecture times
* MVV schedule
* Opening times
* Campus maps
* Important links
* RSS-feeds
* Automatic muting of the phone during lectures
* Access to TUMonline

Bugsense has been removed for F-droid distribution.
Status: Active Development.
.

Repo Type:git
Repo:https://github.com/TCA-Team/TumCampusApp.git

Build:1.1.2,32
    commit=956e42b30a092a28c1748f5152d6779ca4399100
    subdir=TumCampusApp
    patch=bugsense.patch
    prebuild=find src/ -type f |xargs -n 1 perl -pi -e "s/.*package(?=\W)/package/g" && \
        sed -i '30s/package:/Uri uri = Uri.parse("package:/g' src/de/tum/in/tumcampusapp/tumonline/TUMOnlineConst.java & &rm libs/bugsense*

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.2
Current Version Code:32

