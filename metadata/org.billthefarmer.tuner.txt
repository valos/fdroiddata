Categories:Multimedia
License:GPLv3
Web Site:https://github.com/billthefarmer/tuner/wiki
Source Code:https://github.com/billthefarmer/tuner
Issue Tracker:https://github.com/billthefarmer/tuner/issues

Auto Name:Tuner
Summary:Musical instrument pitch tuning
Description:
Accordion tuner with scope, spectrum and strobe. Can show up to eight
different notes or reeds concurrently.
.

Repo Type:git
Repo:https://github.com/billthefarmer/tuner

Build:Version 1.0,1
    commit=v1.0

Build:1.01,101
    commit=v1.01

Build:1.02,102
    commit=v1.02

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.02
Current Version Code:102

